# Django Test App

You can view a working version of this app
[here](http://andrewbalakirev.pythonanywhere.com/)


## Building

It is best to use the python `virtualenv` tool to build locally:

```sh
$ virtualenv-3-7 venv
$ source venv/bin/activate
$ cd warning_devices/
$ pip install -r requirements.txt
$ ./manage.py makemigrations
$ ./manage.py migrate
$ ./manage.py runserver
```

Then visit `http://localhost:8000` to view the app.



## URLs
```
# Base urls
http://localhost:8000/ - list of all devices
http://localhost:8000/device/<int:pk>/ - detail device
http://localhost:8000/device/create/ - create device


# api urls
http://localhost:8000/api/devices/ - list of all devices
http://localhost:8000/api/devices/<int:pk>/ - detail device


# api filters
http://localhost:8000/api/devices/?name__icontains=$NAME&adress__icontains=$ADRESS&latitude__gte=$LATITUDE_GTE&latitude__lte=$LATITUDE_LTE&longitude__gte=$LONGTUDE_GTE&longitude__lte=$LONGTUDE_LTE&radius__gte=$RADIUS_GTE&radius__lte=$RADIUS_lTE&type=$TYPE

$NAME - device name part
$ADRESS - adress name part
$LATITUDE_GTE - Latitude greater than or equal
$LATITUDE_LTE - Latitude less than or equal
$LONGTUDE_GTE - Longtude greater than or equal
$LONGTUDE_LTE - Longtude less than or equal
$RADIUS_GTE - Radius greater than or equal
$RADIUS_LTE - Radius less than or equal


# api permitted methods
http://localhost:8000/api/devices/
	GET - all users
	POST(add new device) - only admins
		data = {
			"name": "name",
			"type": "s(siren)/m(megaphone)",
			"adress": "adress",
			"latitude": "latitude",
			"longitude": "longitude",
			"radius": radius
		}

http://localhost:8000/api/devices/<int:pk>/
	GET - all users
	PUT(device editing) - only admins
	PATCH(partial device editing) - only admins
	DELETE(delete device) - only admins


# getting authorization token
http://localhost:8000/api-token-auth/
	POST - all users
		data = {
			"username": "username",
			"password": "password"
		}


```


