from django.contrib import admin
from django.contrib.auth.models import Group

from .models import Device

# Register your models here.

admin.site.unregister(Group)


@admin.register(Device)
class PostOptions(admin.ModelAdmin):
    fields = ('name', 'type', 'adress', 'latitude', 'longitude', 'radius')
    list_display = ('name', 'type', 'adress',)
    list_filter = ('name', 'type', 'adress', 'latitude', 'longitude', 'radius')
    view_on_site = True


