from device.models import Device
from rest_framework import serializers



class DeviceSerializer(serializers.HyperlinkedModelSerializer):
	
    class Meta:
        model = Device
        fields = ('name', 'type', 'adress', 'latitude', 'longitude', 'radius')
