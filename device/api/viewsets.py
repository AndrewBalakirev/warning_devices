from rest_framework import viewsets
from django_filters import rest_framework as filters

from .permissions import IsAdminUserOrReadOnly
from device.models import Device
from .serializers import DeviceSerializer



class DeviceFilter(filters.FilterSet):
	type = filters.ChoiceFilter(choices=Device.TYPE_CHOICES)

	class Meta:
		model = Device
		fields = {
			'name': ['icontains'],
			'adress': ['icontains'],
			'latitude': ['gte', 'lte'],
			'longitude': ['gte', 'lte'],
			'radius': ['gte', 'lte'],
		}


class DeviceViewSet(viewsets.ModelViewSet):
	serializer_class = DeviceSerializer
	queryset = Device.objects.all()
	filterset_class = DeviceFilter
	permission_classes = [IsAdminUserOrReadOnly]
