from django.db import models
from django.shortcuts import reverse



class Device(models.Model):
    TYPE_CHOICES = (
        ('s', 'Сирена'),
        ('m', 'Громкоговоритель'),
    )

    name = models.CharField(max_length=200, verbose_name='Название')
    type = models.CharField(max_length=1, choices=TYPE_CHOICES, verbose_name='Тип устройства', default='siren')
    adress = models.CharField(max_length=200, verbose_name='Адрес')
    latitude = models.DecimalField(max_digits=9999, decimal_places=6, verbose_name='Широта')
    longitude = models.DecimalField(max_digits=9999, decimal_places=6, verbose_name='Долгота')
    radius = models.PositiveIntegerField(verbose_name='Радиус зоны звукопокрытия')

    def get_absolute_url(self):
        return reverse('device:device_detail', kwargs={'pk': self.pk})

    def __str__(self):
    	return self.name

    class Meta:
        verbose_name_plural = 'Устройства'
        verbose_name = 'Устройство'
        ordering = ['-radius']


