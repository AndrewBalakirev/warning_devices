from django.urls import path, include

from .views import DeviceListView, DeviceDetailView, DeviceCreateView


app_name  = 'device'

urlpatterns = [
    path('', DeviceListView.as_view(), name='device_list'),
    path('device/<int:pk>/', DeviceDetailView.as_view(), name='device_detail'),
    path('device/create/', DeviceCreateView.as_view(), name='device_create'),
]







