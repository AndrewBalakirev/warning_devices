from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView

from .models import Device



class DeviceListView(ListView):
	model = Device
	template_name = 'devices/list.html'
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(DeviceListView, self).get_context_data(**kwargs)
		all_devices = Device.objects.all()
		type = self.request.GET.get('type')
		filter_devices_set = all_devices.filter(type=type)
		context['filter_set'] = filter_devices_set
		return context


class DeviceDetailView(DetailView):
	model = Device
	template_name = 'devices/detail.html'


class DeviceCreateView(CreateView):
	model = Device
	fields = ('name', 'type', 'adress', 'latitude', 'longitude', 'radius')
	template_name = 'devices/create.html'


