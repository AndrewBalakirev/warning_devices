Django==2.2.1
djangorestframework==3.9.4
psycopg2==2.8.2
psycopg2-binary==2.8.2
django-filter==2.2.0
django-filters==0.2.1
mysqlclient==1.4.4
