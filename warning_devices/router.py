from rest_framework import routers

from device.api.viewsets import DeviceViewSet


router = routers.DefaultRouter()
router.register('devices', DeviceViewSet)
